import { MongoClient } from 'mongodb';

export default class DatabaseClient {

    constructor(name) {
        this.name = name;
    }

    async connect(uri) {
        const client = new MongoClient(uri, { useUnifiedTopology: true });
        await client.connect();

        return client.db(this.name);
    }

}
