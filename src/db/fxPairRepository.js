
const COLLECTION = 'fxpairs';

export default class FxPairRepository {

    static async create(db, pair, fee) {
        await db.collection(COLLECTION).replaceOne(
            { pair }, 
            { pair, fee: fee }, 
            { upsert: true }
        );
    }

    static async update(db, pair, fee) {
        const res = await db.collection(COLLECTION).updateOne(
            { pair },
            { $set: { fee: fee } }
        );
        return res.matchedCount === 1;
    }

    static async getAll(db) {
        const cursor = await db.collection(COLLECTION).find({}, { projection: { _id: 0 } });
        return cursor.toArray();
    }

}
