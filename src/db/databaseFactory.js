import DatabaseMemoryServer from './databaseMemoryServer.js';
import DatabaseClient from './databaseClient.js';

export default class DatabaseFactory {

    static async createConnection() {
        const dbPort = parseInt(process.env.DATABASE_PORT);
        const dbName = process.env.DATABASE_NAME;

        const dbServer = await (new DatabaseMemoryServer(dbPort, dbName)).spawn();

        const dbClient = new DatabaseClient(dbName);
        return dbClient.connect(dbServer.getUri());
    }

}
