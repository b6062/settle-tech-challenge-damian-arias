import { MongoMemoryServer } from 'mongodb-memory-server';

export default class DatabaseMemoryServer {

    constructor(port, name) {
        this.port = port;
        this.name = name;
    }

    async spawn() {
        try {
            const db = await MongoMemoryServer.create({
                instance: { port: this.port, dbName: this.name }
            });
            console.info("MongoDb running at:", db.getUri());
            return db;
        } catch (err) {
            console.error(err);
        }
    }

}
