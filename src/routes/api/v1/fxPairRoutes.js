import Joi from 'joi';
import FxPairController from '../../../controllers/api/v1/fxPairController.js';

const API_VERSION = '/v1';

export default [
    {
        path: API_VERSION+'/fxpair',
		method: 'POST',
		options: {
			handler: FxPairController.createFxPair,
            description:"Create new FX rate pair",
        	notes:'Create new FX rate pair',
            tags:['api'],
            validate: {
                payload: Joi.object({
                    pair: Joi.string().required(),
                    markupFee: Joi.number()
                })
            },
		}
    },
    {
        path: API_VERSION+'/fxpair/{pair}',
		method:'PUT',
		options: {
			handler: FxPairController.updateFxPair,
            description:"Update existent FX rate pair",
        	notes:'Update existent FX rate pair',
            tags:['api'],
            validate: {
                params: Joi.object({
                    pair: Joi.string().required()
                }),
                payload: Joi.object({
                    markupFee: Joi.number()
                })
            },
		}
    },
    {
        path: API_VERSION+'/fxpairs',
		method:'GET',
		options: {
			handler: FxPairController.getFxRates,
            description:"Get a list of all registered FX pairs with their updated rates",
        	notes:'Get a list of all registered FX pairs with their updated rates',
            tags:['api'],
		}
    }
];
