import InvalidValueException from '../exceptions/invalidValueException.js'

// Entity class that encapsulates the business logic for FX pairs
export default class FxPair {

    constructor(pair, fee, rate) {
        this.pair = FxPair.validatePair(pair || '');
        this.markupFee = FxPair.validateMarkupFee(fee);
        this.originalRate = rate;
        if(rate && fee) {
            this.feeAmount = this.originalRate * this.markupFee;
            this.ratePlusFee = this.originalRate + this.feeAmount;
        }
    }

    static validatePair(val) {
        if(val.length !== 6) throw new InvalidValueException("Invalid FX pair: "+val);
        return val;
    }

    static validateMarkupFee(val) {
        if(val < 0 || val > 1) throw new InvalidValueException("Invalid markup fee: "+val);
        return val;
    }

}
