import Boom from '@hapi/boom';

import InvalidValueException from '../exceptions/invalidValueException.js'

// This should be the place where all errors are processed
export default class ErrorHandler {

    // there's probably a library that does this better, but you get the idea
    static enclose(callback) {
        try {
            return callback();
        } catch (err) {
            // console.error(err);
            if(err instanceof  InvalidValueException) {
                throw Boom.badRequest(err.message);
            }
        }
    }

}
