import Boom from '@hapi/boom';

import ErrorHandler from '../../errorHandler.js'
import FxPair from '../../../models/fxPair.js';
import FxPairRepository from '../../../db/fxPairRepository.js';

export default class FxPairController {

    static async createFxPair(request, h) {
        const pair = request.payload.pair;
        const fee = request.payload.markupFee;
        const fxPair = ErrorHandler.enclose(() => new FxPair(pair, fee));
        await FxPairRepository.create(this.db, pair, fee);
        return h.response(fxPair).code(201);
    }

    static async updateFxPair(request, h) {
        const pair = request.params.pair;
        const fee = request.payload.markupFee;
        const fxPair = ErrorHandler.enclose(() => new FxPair(pair, fee));
        if(await FxPairRepository.update(this.db, pair, fee)) {
           return fxPair;
        } else {
            throw Boom.notFound("The FX pair '"+pair+"' does not exists");
        }
    }

    static async getFxRates(request, h) {
        let rates = await FxPairRepository.getAll(this.db);
        return await Promise.all(rates.map(async rate => 
            // since rates vary very often, we get the latest rates before returning them
            new FxPair(rate.pair, rate.fee, await this.fixer.getRateForPair(rate.pair))
        ));
    }

}
