import Hapi from '@hapi/hapi';
import Inert from '@hapi/inert';
import Vision from '@hapi/vision';
import HapiSwagger from 'hapi-swagger';

const swaggerOptions = {
    info: {
        title: 'Settle challenge API Documentation',
    }
};

export default class ServerBuilder {

    constructor(host, port) {
        this.server = new Hapi.Server({
            host: host,
            port: port
        });
        this.plugins = [ Inert, Vision ];
        this.routes = [];
        this.context = {};
    }

    registerSwaggerPlugin() {
        this.plugins.push({
            plugin: HapiSwagger,
            options: swaggerOptions
        });
        return this;
    }

    addToContext(obj) {
        this.context = Object.assign(this.context, obj);
        return this;
    }

    addRoute(route) {
        this.routes = this.routes.concat(route);
        return this;
    }

    async build() {
        await this.server.register(this.plugins);

        this.server.bind(this.context);

        this.server.route(this.routes);

        this.server.events.on('response', function (request) {
            console.log(request.info.remoteAddress + ': ' + request.method.toUpperCase() + ' ' + request.path + ' --> ' + request.response.statusCode);
        });
    
        return this.server;
    }

}
