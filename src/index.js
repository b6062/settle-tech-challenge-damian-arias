import dotenv from 'dotenv'

import ServerBuilder from './server/serverBuilder.js';
import DatabaseFactory from './db/databaseFactory.js';
import FixerServiceFactory from './services/external/fixerServiceFactory.js';
import fxPairRoutes from './routes/api/v1/fxPairRoutes.js';

dotenv.config();

async function init() {
    const server = await new ServerBuilder(process.env.SERVER_HOST, process.env.SERVER_PORT)
        .registerSwaggerPlugin()
        .addRoute(fxPairRoutes)
        .addToContext({ db: await DatabaseFactory.createConnection() })
        .addToContext({ fixer: FixerServiceFactory.create() })
        .build();

    try {
        await server.start();
        console.log('Server running at:', server.info.uri);
    } catch(err) {
        console.log(err);
    }
}

process.on('unhandledRejection', err => {
    console.log(err);
    process.exit(1);
});

init();
