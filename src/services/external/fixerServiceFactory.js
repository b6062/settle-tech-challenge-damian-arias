import FixerService from './fixerService.js';

export default class FixerServiceFactory {

    static create() {
        return new FixerService(
            process.env.FIXERIO_URL,
            process.env.FIXERIO_API_KEY,
            process.env.FIXERIO_CACHE_LIFESPAN_MS,
            process.env.FIXERIO_SUPPORTED_SYMBOLS
        );
    }

}
