import { URL } from 'url';
import got from 'got';
import cache from 'memory-cache';

const RATES_PATH = "/api/latest";
const BASE_SYMBOL = 'EUR';

export default class FixerService {

    constructor(url, apiKey, cacheLs, symbols) {
        this.url = url;
        this.apiKey = apiKey;
        this.cache = new cache.Cache();
        this.cacheLifespan = parseInt(cacheLs);
        this.symbols = symbols;
    }

    async getRateForPair(pair) {
        let fixerRes = this.cache.get('rates');
        if(!fixerRes) {
            fixerRes = await this._getLatestsRatesFromFixer();
            if(fixerRes.success) {
                this.cache.put('rates', fixerRes, this.cacheLifespan);
            } else {
                console.log("Fixer API has issues returning latest rates", fixerRes);
            }
        }
        // console.log(fixerRes)
        return this._calculateRateForPair(fixerRes.rates, pair);
    }

    async _getLatestsRatesFromFixer() {
        let url = new URL(this.url + RATES_PATH);
        url.searchParams.append('access_key', this.apiKey);
        url.searchParams.append('base', BASE_SYMBOL);
        url.searchParams.append('symbols', this.symbols);
        try {
            // console.log(url.href)
            let response = await got(url.href, { responseType: 'json' });
            return response.body;
        } catch(err) {
            console.log("Error requesting Fixer API", err);
		}
    }

    _calculateRateForPair(rates, pair) {
        const from = pair.substring(0, 3);
        const to = pair.substring(3, 6);
        return rates[BASE_SYMBOL] * rates[to] / rates[from];
    }

}
